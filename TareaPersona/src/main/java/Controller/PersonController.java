/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Sistemas-37
 */
public class PersonController implements ActionListener, FocusListener{
    PersonForm personFrame;
    JComboBox dComboBox;
    JComboBox mComboBox;
    JFileChooser d;
    Persona persona;
    String[] departments = new String[] { 
        "Boaco","Carazo","Chinandega","Chontales",
        "Costa Caribe Norte","Costa Caribe Sur",
        "Estelí","Granada","Jinotega","León",
        "Madriz","Managua","Masaya","Matagalpa",
        "Nueva Segovia","Río San Juan","Rivas" };
    String[] masayaMunicipalities = new String[] { 
        "Masaya","Nindirí", "Catarina", "Masatepe", 
        "La concepción", "Tisma","Nandasmo", 
        "San Juan de Oriente" };
    String[] managuaMunicipalities = new String[] { 
        "Managua","Ciudad Sandino", "El crucero", 
        "Mateare", "San Francisco Libre", 
        "San Rafael del Sur","Ticuantepe", "Tipitapa" };
    String[] granadaMunicipalities = new String[] { 
        "Granada","Diriomo", "Diriá", "Nandaime"};
    String[] leonMunicipalities = new String[] { 
        "León","Achuapa", "El Jicaral", 
        "La Paz Centro","Larreynaga",
        "Nagarote","Quezalguaque"};
    String[] rivasMunicipalities = new String[] { 
        "Rivas","Altagracia", "Belén", "Buenos Aires",
        "Cárdenas","Moyogalpa","Potosí", "San Jorge"};
    DefaultComboBoxModel departmentComboBoxModel = new DefaultComboBoxModel<>(departments);
    
    public PersonController(PersonForm f){
        super();
        personFrame = f;
        d = new JFileChooser();
        persona = new persona();
    }
   
    public void setPerson(Persona b){
        persona = b;
    }
    public void setPerson(String filePath){
        File f = new File(filePath);
        readPerson(f);
    }
    public Persona getPerson(){
        return persona;
    }     
    public void setDepartmentComboBox(JComboBox jcombo){
        dComboBox = jcombo;
        dComboBox.setModel(departmentComboBoxModel);
    }
    public void setMunicipalityComboBox(JComboBox jcombo){
        mComboBox = jcombo;
    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().getClass().getName().equals(JComboBox.class.getName()))
        {
            JComboBox cbo = (JComboBox)e.getSource();
            switch(cbo.getName()){
                case "department":
                    switch(cbo.getSelectedItem().toString()){
                        case "Managua":
                            mComboBox.setModel(new DefaultComboBoxModel(managuaMunicipalities));
                            break;
                        case "Masaya":
                            mComboBox.setModel(new DefaultComboBoxModel(masayaMunicipalities));
                            break;
                        case "Granada":
                            mComboBox.setModel(new DefaultComboBoxModel(granadaMunicipalities));
                            break;
                        case "León":
                            mComboBox.setModel(new DefaultComboBoxModel(leonMunicipalities));
                            break;
                        case "Rivas":
                            mComboBox.setModel(new DefaultComboBoxModel(rivasMunicipalities));
                            break;
                        default:
                            mComboBox.setModel(new DefaultComboBoxModel(new String[]{}));
                            break;
                    }
                    break;
            }
        }
        else
        {
        switch (e.getActionCommand()){
            case "Save":
                d.showSaveDialog(personFrame); 
                persona = personFrame.getPersonData();
                writePerson(d.getSelectedFile());
                break;
            case "Open":                
                d.showOpenDialog(personFrame);  
                persona = readPerson(d.getSelectedFile());
                personFrame.setPersonData(persona);
                break;
            case "Clear":
                personFrame.clear();
                break;

        }
        }
    }
    private void writePerson(File file){
         try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file)); 
            w.writeObject(getPerson());
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(PersonController.class.getName()).log(Level.SEVERE, null, ex);
        }       
    }
    private Persona readPerson(File file){
         try{
            ObjectInputStream ois =  new ObjectInputStream(new FileInputStream(file));
            return (Persona)ois.readObject();
        }
        catch(FileNotFoundException e){
            JOptionPane.showMessageDialog(personFrame, e.getMessage(),personFrame.getTitle(),JOptionPane.WARNING_MESSAGE);
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(PersonController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    private Persona[] readPersonList(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        Persona[] persons;
        try(FileInputStream in = new FileInputStream(file);
            ObjectInputStream s = new ObjectInputStream(in)) {
            persons = (Persona[]) s.readObject();
        }
        return persons;
    }
    @Override
    public void focusGained(FocusEvent e) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent e) {
        if(e.getOppositeComponent().getClass().getName().endsWith("JTextField")){
            switch(((javax.swing.JTextField) e.getSource()).getName()){
                case "idTextField":
                    System.out.println("Obtenido desde: idTextField" + ", valor: " +((javax.swing.JTextField) e.getSource()).getText());
                    break;
                default:
                    System.out.println("Obtenido desde: " + ((javax.swing.JTextField) e.getSource()).getName());
                    break;
            }
        }
    }

    private static class PersonForm {

        public PersonForm() {
        }
        
        private void setPersonData(Persona persona) {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private Persona getPersonData() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

        private void clear() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    }
    
}
